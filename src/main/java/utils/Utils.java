package utils;

import java.util.Base64;

public class Utils {
    public static String decode64(String encodedStr) {
        Base64.Decoder decoder = Base64.getDecoder();
        return new String(decoder.decode(encodedStr.getBytes()));
    }

    public static String encode64(String str) {
        Base64.Encoder encoder = Base64.getEncoder();
        return new String((encoder.encode(str.getBytes())));
    }
}
